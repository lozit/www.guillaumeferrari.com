---
title: Workshop
name: workshop
weight: 10
URL: "/workshop/"
---

<h1>Film Photography Workshop</h1>

{{< figure src="/workshop.jpg" title="" >}}

I am doing a workshop about the use of film for the wedding photographer.

The goal of this workshop is to help wedding photographers to switch from full digital to full film (or hybrid).

No marketing, no artistic advice, only technical and practical informations.

If you want more informations, just ask !