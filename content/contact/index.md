---
title: Contact
---

<h1>Hello, is it me you're looking for ?</h1>

{{< figure src="/contact.jpg" title="" >}}

You can contact me by phone (+33 6 85 68 81 61), by mail (<a href="mailto:guillaume.ferrari@gmail.com">guillaume.ferrari@gmail.com</a>) or by using the form bellow.
 
Photography courtesy of my father. Look at this fantastic wallpaper ! (i'm on the left by the way).

<form name="contact" method="POST" netlify>
  <p>
    <label>Your Name: <input type="text" name="name" /></label>   
  </p>
  <p>
    <label>Your Email: <input type="email" name="email" /></label>
  </p>
  <p>
    <label>Message: <textarea name="message"></textarea></label>
  </p>
  <p>
    <button type="submit">Send</button>
  </p>
</form>