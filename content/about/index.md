---
title: About
name: About
weight: 10
URL: "/about/"
---

<h1>About</h1>

{{< figure src="/about.jpg" title="" >}}

I like watching pictures from previous weddings I covered.

I pass through them and remember. I smile all alone in front of my screen.

I like to imagine my married do the same: watch and remember.

It pleases me to see and capture the little things that tell the story of your day. Your story.

But I've realized that it's not just that that i like: A wedding is a dose of positive emotions ... and I think I am addicted.

And these emotions, this is what I want to transcribe in picture.