---
title: Pricing
---

<h1>Pricing</h1>

{{< figure src="/pricing.jpg" title="" >}}

I'm a french photographer based in Provence. I am available for shooting worldwide. 

Family and Couple session start at 1100€ with a book / Wedding start at 2800€.

I'm shooting only with film camera because it's the way i found to have the pictures i like.

I'm a candid photographer, i like spontaneous pictures because they are real and i think we are bombed by fake picture every day and we all deserve a bit of reality. 

Contact me for more informations or full pricing list.