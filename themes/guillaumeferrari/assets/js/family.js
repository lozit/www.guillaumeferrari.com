new Vue({
  el: 'image-slider',
  data: {
      images: ['/img/family/7977718739_08b7a81779_o.jpg', '/img/family/15256062751_a26d974c28_o.jpg', '/img/family/16004566603_8ed415d80c_o.jpg'
      ,'/img/family/18349349056_e6fff31a1f_o.jpg', '/img/family/20477044180_8c68467fa0_k.jpg', '/img/family/20652518966_0ca1dea216_k.jpg',
    '/img/family/21492562558_8c12060721_k.jpg','/img/family/27619193691_570acd4e07_o.jpg'],
      currentNumber: 0,
      timer: null
  },

  mounted: function () {
      this.startRotation();
  },

  methods: {
      startRotation: function() {
          this.timer = setInterval(this.next, 3000);
      },

      stopRotation: function() {
          clearTimeout(this.timer);
          this.timer = null;
      },

      next: function() {
          this.currentNumber += 1
      },
      prev: function() {
          this.currentNumber -= 1
      }
  },
  
  computed: {
    currentImage: function() {
      return this.images[Math.abs(this.currentNumber) % this.images.length];
    }
  }
});